import "./App.css";
import Header from "./components/organisms/Header";
import Content from "./components/organisms/Content";
import Footer from "./components/organisms/Footer";
import Container from "react-bootstrap/Container";
import HeaderMobile from "./components/organisms/HeaderMobile";
import FooterMobile from "./components/organisms/FooterMobile";

function App() {
  return (
    <>
      <Container>
        {/* Web Version */}
        <div className="d-none d-md-block">
          <Header />
        </div>
        {/* Mobile Version */}
        <div className="d-md-block d-md-none">
          <HeaderMobile />
        </div>

        <Content />

        {/* Web Version */}
        <div className="d-none d-md-block">
          <Footer />
        </div>
        {/* Mobile Version */}
        <div className="d-md-block d-md-none">
          <FooterMobile />
        </div>
      </Container>
    </>
  );
}

export default App;
