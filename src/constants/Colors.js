const Colors = {
  PRIMARY: "#963E45",
  SECONDARY: "#F4E3CF",
};

export default Colors;
