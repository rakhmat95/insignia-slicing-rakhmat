import React from "react";
import styled from "styled-components";
import { Colors } from "../../constants";
import { BiSearch } from "react-icons/bi";
import Form from "react-bootstrap/Form";

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  border-radius: 2;
  width: 100%;
  background-color: ${Colors.SECONDARY};
  margin-right: 1em;
`;

function SearchBar(props) {
  return (
    <Wrapper>
      <Form.Control
        style={{
          backgroundColor: Colors.SECONDARY,
          color: Colors.PRIMARY,
          borderWidth: 0,
          height: "2.5rem",
        }}
        placeholder="Find..."
      />
      <BiSearch
        size={20}
        color={Colors.PRIMARY}
        style={{ alignSelf: "center", marginRight: 10 }}
      />
    </Wrapper>
  );
}

export default SearchBar;
