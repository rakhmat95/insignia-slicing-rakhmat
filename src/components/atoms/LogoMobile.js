import React from "react";
import styled from "styled-components";

const TextSocial = styled.span`
  font-size: 20px;
  font-family: Segoe UI;
  font-weight: bold;
`;

const TextNetwork = styled.span`
  font-size: 20px;
  font-family: Segoe UI;
`;

function LogoMobile(props) {
  return (
    <div>
      <TextSocial>Social</TextSocial>
      <TextNetwork>Network</TextNetwork>
    </div>
  );
}

export default LogoMobile;
