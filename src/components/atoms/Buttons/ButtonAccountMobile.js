import React from "react";
import Button from "react-bootstrap/Button";
import { Colors } from "../../../constants";
import { IoMdPerson } from "react-icons/io";

function ButtonAccountMobile(props) {
  return (
    <Button
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <IoMdPerson size={24} color={Colors.SECONDARY} />
    </Button>
  );
}

export default ButtonAccountMobile;
