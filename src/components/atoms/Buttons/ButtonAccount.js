import React from "react";
import Button from "react-bootstrap/Button";
import { Colors } from "../../../constants";
import { IoMdPerson } from "react-icons/io";
import styled from "styled-components";

const WrapperName = styled.div`
  display: flex;
  flex-direction: column;
`;

const TextFirstName = styled.span`
  font-weight: bold;
  font-size: 20px;
  font-family: Segoe UI;
`;

const TextLasttName = styled.span`
  font-size: 14px;
  font-family: Segoe UI;
`;

function ButtonAccount(props) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <Button
        style={{
          display: "flex",
          backgroundColor: Colors.SECONDARY,
          color: Colors.PRIMARY,
          borderWidth: 0,
          borderRadius: 2,
          alignItems: "center",
          marginRight: "1em",
          width: "2.5rem",
          height: "2.5rem",
        }}
      >
        <IoMdPerson size={20} color={Colors.PRIMARY} />
      </Button>
      <WrapperName>
        <TextFirstName>Washeem</TextFirstName>
        <TextLasttName>Arshad</TextLasttName>
      </WrapperName>
    </div>
  );
}

export default ButtonAccount;
