import React from "react";
import Button from "react-bootstrap/Button";
import { Colors } from "../../../constants";
import { HiOutlineUpload } from "react-icons/hi";

function ButtonUpload(props) {
  return (
    <Button
      style={{
        display: "flex",
        backgroundColor: Colors.SECONDARY,
        color: Colors.PRIMARY,
        borderWidth: 0,
        borderRadius: 2,
        alignItems: "center",
        fontSize: "16px",
        padding: "0.5rem",
        paddingLeft: "1.5rem",
        paddingRight: "1.5rem",
        marginRight: "1em",
        height: "2.5rem",
      }}
    >
      <HiOutlineUpload
        size={18}
        color={Colors.PRIMARY}
        style={{ marginRight: 10 }}
      />
      Upload
    </Button>
  );
}

export default ButtonUpload;
