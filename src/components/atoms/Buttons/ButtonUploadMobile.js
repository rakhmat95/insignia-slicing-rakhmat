import React from "react";
import Button from "react-bootstrap/Button";
import { Colors } from "../../../constants";
import { BiUpload } from "react-icons/bi";

function ButtonUploadMobile(props) {
  return (
    <Button
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <BiUpload size={24} color={Colors.SECONDARY} />
    </Button>
  );
}

export default ButtonUploadMobile;
