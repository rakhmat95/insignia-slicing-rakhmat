import React from "react";
import Button from "react-bootstrap/Button";
import { Colors } from "../../../constants";
import { GiHamburgerMenu } from "react-icons/gi";

function ButtonMenu(props) {
  return (
    <Button
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <GiHamburgerMenu size={24} color={Colors.SECONDARY} />
    </Button>
  );
}

export default ButtonMenu;
