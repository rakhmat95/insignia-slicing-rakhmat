import React from "react";
import { Breadcrumb } from "react-bootstrap";
import styled from "styled-components";
import { Colors } from "../../constants";

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  padding-top: 10px;
  border-top: 1px solid ${Colors.SECONDARY};
  border-bottom: 1px solid ${Colors.SECONDARY};
`;

function NavBar(props) {
  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item href="#" active style={{ color: Colors.SECONDARY }}>
          Videos
        </Breadcrumb.Item>
        <Breadcrumb.Item href="#" active style={{ color: Colors.SECONDARY }}>
          People
        </Breadcrumb.Item>
        <Breadcrumb.Item href="#" active style={{ color: Colors.SECONDARY }}>
          Documents
        </Breadcrumb.Item>
        <Breadcrumb.Item href="#" active style={{ color: Colors.SECONDARY }}>
          Events
        </Breadcrumb.Item>
        <Breadcrumb.Item href="#" active style={{ color: Colors.SECONDARY }}>
          Communitites
        </Breadcrumb.Item>
        <Breadcrumb.Item href="#" active style={{ color: Colors.SECONDARY }}>
          Favorite
        </Breadcrumb.Item>
        <Breadcrumb.Item href="#" active style={{ color: Colors.SECONDARY }}>
          Channels
        </Breadcrumb.Item>
      </Breadcrumb>
    </Wrapper>
  );
}

export default NavBar;
