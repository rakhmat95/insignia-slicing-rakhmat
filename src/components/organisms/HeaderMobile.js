import React from "react";
import { Row, Col } from "react-bootstrap";
import LogoMobile from "../atoms/LogoMobile";
import SearchBar from "../atoms/SearchBar";
import ButtonAccountMobile from "../atoms/Buttons/ButtonAccountMobile";
import ButtonUploadMobile from "../atoms/Buttons/ButtonUploadMobile";
import ButtonMenu from "../atoms/Buttons/ButtonMenu";

function HeaderMobile(props) {
  return (
    <>
      <Row
        className="py-2"
        style={{
          alignItems: "center",
        }}
      >
        <Col xxs="4" xs="4" sm="4" md="4">
          <ButtonMenu />
        </Col>
        <Col xxs="4" xs="4" sm="4" md="4">
          <LogoMobile />
        </Col>
        <Col
          xxs="4"
          xs="4"
          sm="4"
          md="4"
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
          }}
        >
          <ButtonUploadMobile />
          <ButtonAccountMobile />
        </Col>
      </Row>
      <Row className="mb-2">
        <Col>
          <SearchBar />
        </Col>
      </Row>
    </>
  );
}

export default HeaderMobile;
