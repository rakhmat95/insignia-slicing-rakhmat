import React from "react";
import { RiFacebookFill, RiLinkedinFill, RiTwitterFill } from "react-icons/ri";
import { Colors } from "../../constants";

function Footer(props) {
  return (
    <footer className="footer mt-auto py-3">
      <div>
        <span
          style={{
            display: "flex",
            backgroundColor: Colors.SECONDARY,
            width: "100%",
            height: 1,
            marginBottom: 16,
          }}
        />
        <div className="mb-4" style={{ display: "flex", flexDirection: "row" }}>
          <span
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.SECONDARY,
              padding: 6,
              width: "2rem",
              marginRight: 10,
            }}
          >
            <RiTwitterFill size={20} color={Colors.PRIMARY} />
          </span>
          <span
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.SECONDARY,
              padding: 6,
              width: "2rem",
              marginRight: 10,
            }}
          >
            <RiLinkedinFill size={20} color={Colors.PRIMARY} />
          </span>
          <span
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.SECONDARY,
              padding: 6,
              width: "2rem",
            }}
          >
            <RiFacebookFill size={20} color={Colors.PRIMARY} />
          </span>
        </div>
        <div className="mb-4">
          <span>
            About / For Business / Suggestions / Help & FAQs / Contacts /
            Pricing
          </span>
        </div>
        <div>
          <span>Copyright 2013 companyName int.</span>
        </div>
        <div>
          <span>Privacy / Terms</span>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
