import React from "react";
import { RiFacebookFill, RiLinkedinFill, RiTwitterFill } from "react-icons/ri";
import { Colors } from "../../constants";

function FooterMobile(props) {
  return (
    <footer className="footer mt-auto py-3">
      <div>
        <span
          style={{
            display: "flex",
            backgroundColor: Colors.SECONDARY,
            width: "100%",
            height: 1,
            marginBottom: 16,
          }}
        />
        <div
          className="mb-4"
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <span
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.SECONDARY,
              padding: 6,
              width: "2rem",
              marginRight: 10,
            }}
          >
            <RiTwitterFill size={20} color={Colors.PRIMARY} />
          </span>
          <span
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.SECONDARY,
              padding: 6,
              width: "2rem",
              marginRight: 10,
            }}
          >
            <RiLinkedinFill size={20} color={Colors.PRIMARY} />
          </span>
          <span
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.SECONDARY,
              padding: 6,
              width: "2rem",
            }}
          >
            <RiFacebookFill size={20} color={Colors.PRIMARY} />
          </span>
        </div>
        <div className="mb-4">
          <div className="mb-4 d-flex flex-wrap flex-row justify justify-content-center">
            <span className="mx-2">About</span>
            <span className="mx-2">For Business Suggestions</span>
            <span className="mx-2">Help & FAQs</span>
            <span className="mx-2">Contacts</span>
            <span className="mx-2">Pricing</span>
          </div>
        </div>
        <div className="d-flex justify-content-center">
          <span>Copyright 2013 companyName int.</span>
        </div>
        <div className="d-flex justify-content-center">
          <span>Privacy / Terms</span>
        </div>
      </div>
    </footer>
  );
}

export default FooterMobile;
