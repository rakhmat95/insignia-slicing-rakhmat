import React from "react";
import Card from "./Card";
import CardMobile from "./CardMobile";

function index({ name, views }) {
  return (
    <>
      {/* Web Version */}
      <div className="d-none d-md-block col-4 d-flex flex-column">
        <div className="d-flex flex-row align-items-end justify-content-between">
          <span style={{ fontSize: 24 }}>Channels</span>
          <span style={{ fontSize: 14 }}>Browse all channels</span>
        </div>
        <div className="bg-secondary my-1" style={{ height: 2 }} />
        <div className="d-flex flex-row flex-wrap" style={{ marginTop: 10 }}>
          <Card name={"Google"} />
          <Card name={"Dribble"} />
          <Card name={"Microsoft"} />
          <Card name={"Behance"} isSelected />
          <Card name={"Weather Channel"} />
          <Card name={"Gurus"} />
          <Card name={"iMedia"} />
          <Card name={"Creativeye"} />
          <Card name={"Khan Studios"} />
          <Card name={"Yahoo"} />
          <div className="bg-secondary my-4" style={{ height: 2 }} />
        </div>
      </div>

      {/* Mobile Version */}
      <div
        className="d-md-block d-md-none col-12 d-flex flex-column"
        style={{ marginTop: 16 }}
      >
        <div className="d-flex flex-row align-items-end justify-content-between">
          <span style={{ fontSize: 24 }}>Channels</span>
          <span style={{ fontSize: 14 }}>Browse all channels</span>
        </div>
        <div className="bg-secondary my-1" style={{ height: 2 }} />
        <div className="d-flex flex-row flex-wrap" style={{ marginTop: 10 }}>
          <CardMobile name={"Google"} />
          <CardMobile name={"Dribble"} />
          <CardMobile name={"Microsoft"} />
          <CardMobile name={"Behance"} isSelected />
          <CardMobile name={"Weather Channel"} />
          <CardMobile name={"Gurus"} />
          <CardMobile name={"iMedia"} />
          <CardMobile name={"Creativeye"} />
          <CardMobile name={"Khan Studios"} />
          <CardMobile name={"Yahoo"} />
          <div className="bg-secondary my-4" style={{ height: 2 }} />
        </div>
      </div>
    </>
  );
}

export default index;
