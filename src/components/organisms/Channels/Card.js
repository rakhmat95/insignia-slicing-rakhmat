import React from "react";

function Card({ name, isSelected }) {
  return (
    <div className="d-flex flex-row col-6" style={{ height: "8em" }}>
      <span
        className="fw-bold text-primary bg-secondary d-flex w-100 align-items-end p-1 m-1"
        style={{ fontSize: "1em", marginBottom: 10 }}
      >
        {name}
      </span>
    </div>
  );
}

export default Card;
