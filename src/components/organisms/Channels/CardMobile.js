import React from "react";

function CardMobile({ name, isSelected }) {
  return (
    <div className="d-flex flex-row col-4" style={{ height: "4em" }}>
      <span
        className="fw-normal text-primary bg-secondary d-flex w-100 align-items-center justify-content-center p-1 m-1"
        style={{ fontSize: 14, marginBottom: 10 }}
      >
        {name}
      </span>
    </div>
  );
}

export default CardMobile;
