import React from "react";
import {
  BsFillChatFill,
  BsFillCameraVideoFill,
  BsHandThumbsUpFill,
} from "react-icons/bs";
import Colors from "../../../constants/Colors";

function CardMobile({
  name,
  status,
  message,
  created_at,
  isSelected,
  isChat,
  isLike,
}) {
  return (
    <div
      className="d-flex flex-row p-3"
      style={
        isSelected
          ? {
              border: `2px solid ${Colors.SECONDARY}`,
              backgroundColor: "#A96469",
            }
          : {
              backgroundColor: "#A96469",
            }
      }
    >
      <div className="col-3 bg-secondary" style={{ height: "5rem" }} />
      <div className="col-9" style={{ marginLeft: 16 }}>
        <div className="d-flex flex-row align-items-end">
          <div className="fw-bold w-40" style={{ fontSize: 16 }}>
            {name}{" "}
            <span className="fw-light" style={{ fontSize: "0.8em" }}>
              {status}
            </span>
          </div>
        </div>
        <div className="col-10 text-truncate fw-light my-1">{message}</div>
        <div className="col-8 w-100 d-flex flex-row align-items-center">
          {isChat ? (
            <BsFillChatFill size={18} color={Colors.SECONDARY} />
          ) : isLike ? (
            <BsHandThumbsUpFill size={18} color={Colors.SECONDARY} />
          ) : (
            <BsFillCameraVideoFill size={18} color={Colors.SECONDARY} />
          )}
          <span className="fw-light" style={{ fontSize: 14, marginLeft: 10 }}>
            {created_at}
          </span>
        </div>
      </div>
    </div>
  );
}

export default CardMobile;
