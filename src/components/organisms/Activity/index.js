import React from "react";
import Card from "./Card";
import { BiRightArrowAlt } from "react-icons/bi";
import { Colors } from "../../../constants";
import CardMobile from "./CardMobile";

function index({ name, views }) {
  return (
    <>
      {/* Web Version */}
      <div className="d-none d-md-block col-4 d-flex flex-column">
        <div className="d-flex flex-row align-items-end justify-content-between">
          <span style={{ fontSize: 24 }}>Activity</span>
          <span style={{ fontSize: 14 }}>
            View timeline / Filter activities
          </span>
        </div>
        <div className="bg-secondary my-1" style={{ height: 2 }} />
        <div className="w-100 d-flex flex-column">
          <Card
            created_at={"5 minutes ago"}
            status={"commended"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
            isChat
          />
          <Card
            created_at={"5 minutes ago"}
            status={"added a new video"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
          />
          <Card
            created_at={"5 minutes ago"}
            status={"Shared a document"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
          />
          <Card
            created_at={"5 minutes ago"}
            status={"commended"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
            isSelected
          />
          <Card
            created_at={"5 minutes ago"}
            status={"commended"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
          />
          <Card
            created_at={"5 minutes ago"}
            status={" liked a video"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
            isLike
          />
          <div className="bg-secondary my-4" style={{ height: 2 }} />
        </div>
      </div>

      {/* Mobile Version */}
      <div
        className="d-md-block d-md-none col-12 d-flex flex-column"
        style={{ marginTop: 16 }}
      >
        <div className="d-flex flex-row align-items-end justify-content-between">
          <span style={{ fontSize: 24 }}>Activity</span>
          <span style={{ fontSize: 14 }}>
            View timeline
            <BiRightArrowAlt size={20} color={Colors.SECONDARY} />
          </span>
        </div>
        <div className="bg-secondary my-1" style={{ height: 2 }} />
        <div className="w-100 d-flex flex-column">
          <div style={{ marginBottom: 10 }} />
          <CardMobile
            created_at={"5 minutes ago"}
            status={"commended"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
            isChat
          />
          <div style={{ marginBottom: 10 }} />
          <CardMobile
            created_at={"5 minutes ago"}
            status={"added a new video"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
          />
          <div style={{ marginBottom: 10 }} />
          <CardMobile
            created_at={"5 minutes ago"}
            status={"Shared a document"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
          />
          <div style={{ marginBottom: 10 }} />
          <CardMobile
            created_at={"5 minutes ago"}
            status={"commended"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
            isSelected
          />
          <div style={{ marginBottom: 10 }} />
          <CardMobile
            created_at={"5 minutes ago"}
            status={"commended"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
          />
          <div style={{ marginBottom: 10 }} />
          <CardMobile
            created_at={"5 minutes ago"}
            status={" liked a video"}
            name={"John Stainor"}
            message={"Well, i am liking it how it captures the audio"}
            isLike
          />
          <div className="bg-secondary my-4" style={{ height: 2 }} />
        </div>
      </div>
    </>
  );
}

export default index;
