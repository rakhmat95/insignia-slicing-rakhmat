import React from "react";
import { Row, Col } from "react-bootstrap";
import Logo from "../atoms/Logo";
import SearchBar from "../atoms/SearchBar";
import NavBar from "../atoms/NavBar";
import ButtonUpload from "../atoms/Buttons/ButtonUpload";
import ButtonAccount from "../atoms/Buttons/ButtonAccount";

function Header(props) {
  return (
    <>
      <Row
        className="py-4"
        style={{
          alignItems: "center",
        }}
      >
        <Col lg="4" md="4">
          <Logo />
        </Col>
        <Col
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
          }}
          lg="8"
          md="8"
        >
          <SearchBar />
          <ButtonUpload />
          <ButtonAccount />
        </Col>
      </Row>
      <Row className="mb-2">
        <Col>
          <NavBar />
        </Col>
      </Row>
    </>
  );
}

export default Header;
