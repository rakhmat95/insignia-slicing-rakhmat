import React from "react";
import Videos from "./Videos";
import Activity from "./Activity";
import People from "./People";
import Channels from "./Channels";
import Documents from "./Documents";
import {} from "react-bootstrap";

function Content(props) {
  return (
    <div>
      {/* Web Version */}
      <div className="d-none d-md-block ">
        <div className="d-flex flex-md-row flex-sm-column flex-xs-column flex-xxs-column flex-lg-row">
          <Videos />
          <Activity />
        </div>
      </div>
      {/* Mobile Version */}
      <div className="d-md-block d-md-none">
        <div className=" d-flex flex-column">
          <Videos />
          <Activity />
        </div>
      </div>

      {/* Web Version */}
      <div className="d-none d-md-block">
        <div className="d-flex flex-md-column flex-sm-column flex-xs-column flex-xxs-column flex-lg-row">
          <People />
          <Channels />
        </div>
      </div>
      {/* Mobile Version */}
      <div className="d-md-block d-md-none">
        <div className="d-flex flex-column">
          <People />
          <Channels />
        </div>
      </div>

      <div style={{ marginBottom: 24 }} />

      {/* Web Version */}
      <div className="d-none d-md-block">
        <div className=" d-flex flex-md-column flex-sm-column flex-xs-column flex-xxs-column flex-lg-row">
          <Documents />
        </div>
      </div>
      {/* Mobile Version */}
      <div className="d-md-block d-md-none">
        <div className="d-flex flex-column">
          <Documents />
        </div>
      </div>

      <div style={{ marginBottom: 24 }} />
    </div>
  );
}

export default Content;
