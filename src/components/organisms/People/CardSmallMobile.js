import React from "react";

export default function CardSmallMobile({ name, views }) {
  return (
    <div
      className="d-flex bg-secondary p-2 flex-column justify-content-end"
      style={{ height: "8rem", width: "16rem" }}
    >
      <div className="d-flex justify-content-between align-items-center">
        <span
          className="text-primary text-center"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          {name}
        </span>
        <span
          className="text-primary text-center"
          style={{ fontSize: "0.75rem" }}
        >
          {views} views
        </span>
      </div>
    </div>
  );
}
