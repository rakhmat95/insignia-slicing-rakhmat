import React from "react";
import { BsEye } from "react-icons/bs";
import { Colors } from "../../../constants";

export default function CardUpload({ name, views }) {
  return (
    <div
      className="d-flex w-100 h-100 bg-primary p-2 flex-row align-items-center justify-content-center border border-secondary"
      style={{ height: "8rem" }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          padding: 2,
          width: 30,
          height: 30,
          marginRight: 10,
          border: `2px solid ${Colors.SECONDARY}`,
          borderRadius: 100,
        }}
      >
        <BsEye size={10} color={Colors.SECONDARY} />
      </div>
      <div className="d-flex flex-column">
        <span
          className="text-secondary text-left"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          Show
        </span>
        <span
          className="text-secondary text-left"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          Your Work
        </span>
      </div>
    </div>
  );
}
