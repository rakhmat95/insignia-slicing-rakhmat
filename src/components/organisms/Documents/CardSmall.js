import React from "react";

export default function CardSmall({ name, views }) {
  return (
    <div
      className="d-flex w-100 h-100 bg-secondary p-2 flex-column justify-content-end"
      style={{ height: "8rem" }}
    >
      <div className="d-flex justify-content-between align-items-center">
        <span
          className="text-primary text-center"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          {name}
        </span>
        <span
          className="text-primary text-center"
          style={{ fontSize: "0.75rem" }}
        >
          {views} views
        </span>
      </div>
    </div>
  );
}
