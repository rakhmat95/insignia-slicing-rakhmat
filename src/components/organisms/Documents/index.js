import React from "react";
import CardBig from "./CardBig";
import CardSmall from "./CardSmall";
import CardUpload from "./CardUpload";
import CardBigMobile from "./CardBigMobile";
import CardSmallMobile from "./CardSmallMobile";
import { BiRightArrowAlt } from "react-icons/bi";
import { Colors } from "../../../constants";

function index(props) {
  return (
    <>
      {/* Web Version */}
      <div
        className="d-none d-md-block d-flex col-8 flex-column"
        style={{ paddingRight: 10 }}
      >
        <div className="d-flex flex-row align-items-end justify-content-between w-50">
          <span className="fw-bold" style={{ fontSize: 24 }}>
            Documents
          </span>
          <span className="fw-normal" style={{ fontSize: 14 }}>
            Browse all documents
          </span>
        </div>
        <div className="d-flex flex-row" style={{ marginTop: 16, height: 600 }}>
          <div className="d-flex flex-column w-100" style={{ marginRight: 10 }}>
            <CardBig
              title={"How to improve your skills"}
              name="Washeem Arshad"
              views={"12345"}
            />
            <div className="d-flex flex-row w-100 h-50">
              <CardSmall name="John Stainior" views={"12345"} />
              <div style={{ marginRight: 10 }} />
              <CardSmall name="John Doe" views={"12345"} />
            </div>
          </div>
          <div className="col-4 d-flex flex-column">
            <CardSmall name="Michael Philips" views={"12345"} />
            <div style={{ marginBottom: 10 }} />
            <CardSmall name="Ahmed Yasin" views={"12345"} />
            <div style={{ marginBottom: 10 }} />
            <CardUpload />
          </div>
        </div>
      </div>

      {/* Mobile Version */}
      <div className="d-md-block d-md-none d-flex col-12 flex-row flex-wrap overflow-hidden">
        <div
          className="d-flex flex-row align-items-end justify-content-between w-100"
          style={{ marginBottom: 10 }}
        >
          <span style={{ fontSize: 24 }}>Documents</span>
          <span style={{ fontSize: 14 }}>
            Browses all documents
            <BiRightArrowAlt size={20} color={Colors.SECONDARY} />
          </span>
        </div>

        <div className="d-flex flex-row flex-nowrap overflow-auto">
          <div
            className="card card-block mx-2 bg-secondary"
            style={{ minWidth: 300 }}
          >
            <CardBigMobile
              title={"How to improve your skills"}
              name="Washeem Arshad"
              views={"12345"}
            />
          </div>
          <div
            className="card card-block mx-2 bg-secondary"
            style={{ minWidth: 300 }}
          >
            <CardSmallMobile name="John Stainior" views={"12345"} />
          </div>
          <div
            className="card card-block mx-2 bg-secondary"
            style={{ minWidth: 300 }}
          >
            <CardSmallMobile name="John Doe" views={"12345"} />
          </div>
          <div
            className="card card-block mx-2 bg-secondary"
            style={{ minWidth: 300 }}
          >
            <CardSmallMobile name="Michael Philips" views={"12345"} />
          </div>
          <div
            className="card card-block mx-2 bg-secondary"
            style={{ minWidth: 300 }}
          >
            <CardSmallMobile name="Ahmed Yasin" views={"12345"} />
          </div>
        </div>
      </div>
    </>
  );
}

export default index;
