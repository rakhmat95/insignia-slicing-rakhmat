import React from "react";
import { BsPlusCircle } from "react-icons/bs";
import { Colors } from "../../../constants";

export default function CardUpload({ name, views }) {
  return (
    <div
      className="d-flex w-100 h-100 bg-primary p-2 flex-row align-items-center justify-content-center border border-secondary"
      style={{ height: "8rem" }}
    >
      <BsPlusCircle
        size={30}
        color={Colors.SECONDARY}
        style={{ marginRight: 10 }}
      />
      <div className="d-flex flex-column">
        <span
          className="text-secondary text-left"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          Share
        </span>
        <span
          className="text-secondary text-left"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          Your Document
        </span>
      </div>
    </div>
  );
}
