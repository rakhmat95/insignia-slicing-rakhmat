import React from "react";
import { BsArrowUpCircle } from "react-icons/bs";
import { Colors } from "../../../constants";

export default function CardUpload({ name, views }) {
  return (
    <div
      className="d-flex w-100 h-100 bg-primary p-2 flex-row align-items-center justify-content-center border border-secondary"
      style={{ height: "8rem" }}
    >
      <BsArrowUpCircle
        size={30}
        color={Colors.SECONDARY}
        style={{ marginRight: 10 }}
      />
      <div className="d-flex flex-column">
        <span
          className="text-secondary text-left"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          Upload
        </span>
        <span
          className="text-secondary text-left"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          Your Own Video
        </span>
      </div>
    </div>
  );
}
