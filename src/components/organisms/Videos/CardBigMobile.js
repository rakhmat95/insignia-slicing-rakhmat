import React from "react";

export default function CardBigMobile({ title, name, views }) {
  return (
    <div
      className="d-flex bg-secondary p-2 flex-column justify-content-end"
      style={{ height: "8rem", marginRight: 10, borderRadius: 100 }}
    >
      <span
        className="text-primary"
        style={{ fontWeight: "bold", fontSize: 20 }}
      >
        {title}
      </span>
      <div className="d-flex justify-content-between align-items-center">
        <span
          className="text-primary text-center"
          style={{ fontWeight: "500", fontSize: 16 }}
        >
          {name}
        </span>
        <span className="text-primary text-center" style={{ fontSize: 14 }}>
          {views} views
        </span>
      </div>
    </div>
  );
}
